import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.15

Rectangle {
    id: root
    width: main.width
    height: main.height
    property int numberofproducts : shoppagepy.getProductsListSize()
    property var numberOfColumns: 3
    property int numberOfRows
    property int sortType: 0 //0-none, 1-rating, 2-name, 3-price up, 4 - price down
    color : "white"

    // function hello(){
    //     console.log('hellohellohellohello')
    //     repeater.costyl = 10
    // }

    RowLayout {
        anchors.leftMargin: 20
        anchors.left : parent.left
        z : 2
        id: searchlayout
        width: parent.width
        height: 50


        TextField {
            id : searchbytext
            placeholderText: 'Введите название товара...'
            Layout.preferredWidth: parent.width * 0.9
            onTextChanged: {
                if(searchbytext.length > 2) {
                    shoppagepy.getProductsByNamePart(searchbytext.text)
                    sorting(sortType)
                    main.updateShopPage()
                }
                if(searchbytext.length == 0) {
                    shoppagepy.getAllProducts()
                    filtering()
                    sorting(sortType)
                    main.updateShopPage()
                }
            }
        }

    }
    GridLayout {
        rows: 2
        columns: 4
        id: gridlayout
        anchors.leftMargin: 20
        anchors.left : parent.left
        anchors.rightMargin: 20
        anchors.right : parent.right
        anchors.top : searchlayout.bottom
        width: parent.width
        height: 70

        RoundButton {
            id: filter1button
            Layout.row: 0
            Layout.column: 0
            Layout.preferredWidth: parent.width * 0.2
            Layout.preferredHeight: 50
            text: 'Фильтровать'

            onClicked: {
                filtering()
                main.updateShopPage()
                sorting(sortType)
            }
        }

        TextField {
            id : minprice
            Layout.row: 0
            Layout.column: 1
            Layout.preferredWidth: parent.width * 0.2
            validator: IntValidator {bottom: 1; top: 100000}
            placeholderText: 'Введите минимальную цену...'

        }

        TextField {
            id : maxprice
            Layout.row: 0
            Layout.column: 2
            Layout.preferredWidth: parent.width * 0.2
            validator: IntValidator {bottom: 1; top: 100000}
            placeholderText: 'Введите максимальную цену...'
        }

        RoundButton {
            id: sortbutton
            Layout.row: 0
            Layout.column: 3
            Layout.preferredWidth: parent.width * 0.2
            Layout.preferredHeight: 50
            text: 'Сбросить фильтры'
            onClicked: {
                shoppagepy.getAllProducts()
                minprice.clear()
                maxprice.clear()
                main.updateShopPage()
                sorting(sortType)
                searchbytext.text=''
            }
        }
        Text {
            id: textsortby
            Layout.row: 1
            Layout.column: 0
            text: 'Сортировать по'

        }

        ComboBox {
            id: sortcombobox
            Layout.row: 1
            Layout.column: 1
            Layout.preferredWidth: parent.width * 0.2
            textRole: "key"
            model: ListModel {
                ListElement { key: "умолчанию"}
                ListElement { key: "рейтингу"}
                ListElement { key: "названию"}
                ListElement { key: "цене вверх"}
                ListElement { key: "цене вниз"}
            }
            onActivated: {
                sortType = Qt.binding(function() { return currentIndex })
                sorting(sortType)
            }
        }
    }
    ScrollView {
        id : scrollview
        contentHeight: flowShP.height+300
        anchors.leftMargin: 20
        anchors.left : parent.left
        width: parent.width
        anchors.top: gridlayout.bottom
        anchors.topMargin: 50
        height: parent.height

        clip: true
        Flow {
            id: flowShP
            spacing : 10
            width : root.width
            property var cellHeight: 250
            property var cellWidth: 150

            Repeater {
                id: repeater
                // property int costyl : 5
                model: numberofproducts
                ProductCard {
                    id_product : shoppagepy.getProductId(index)
                    name : shoppagepy.getProductName(index)
                    rating : shoppagepy.getProductRating(index)
                    image_first_card : shoppagepy.getProductImage_first(index)
                    image_second_card : shoppagepy.getProductImage_second(index)
                    price: shoppagepy.getProductPrice(index)
                    start_q_in_basket: shoppagepy.getProductQuantityInBasket(index)
                    quantity: shoppagepy.getProductQuantity(index)
                }
            }
        }
    }
    Connections {
        target: shoppagepy
        function onCleanSignal() {
            root.numberofproducts = 0        
        }
        function onOrderSignal() {
            sorting(sortType)
        }
    }
    function filtering(){
        if(minprice.text == '' && maxprice.text == '')
            shoppagepy.getProductsInPriceBetween(0, 100000000000)
        else if(maxprice.text == '')
            shoppagepy.getProductsInPriceBetween(minprice.text, 100000000000)
        else if(minprice.text == '')
            shoppagepy.getProductsInPriceBetween(0, maxprice.text)
        else
            shoppagepy.getProductsInPriceBetween(minprice.text, maxprice.text)
    }
    function sorting(sortType){
        switch (sortType){
            case 0:
                shoppagepy.getProductsAssorted();
                break;
            case 1:
                shoppagepy.getProductsSortedByRating();
                break;
            case 2:
                shoppagepy.getProductsSortedByName()
                break;
            case 3:
                shoppagepy.getProductsSortedByPrice("up")
                break;
            case 4:
                shoppagepy.getProductsSortedByPrice("down")
                break;

        }
        main.updateShopPage()  
    }
}

