from peewee import *

db = SqliteDatabase('products.db')

class OrdersDto(Model):
    id = PrimaryKeyField(unique = True)
    price = DoubleField()
    deliveryType = TextField()
    address = TextField()
    orderId = IntegerField()

    class Meta:
        database = db
        order_by = 'id'
        db_table = 'orders'
