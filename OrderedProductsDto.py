from peewee import *

db = SqliteDatabase('products.db')

class OrderedProductsDto(Model):
    id = PrimaryKeyField(unique = True)
    productName = TextField()
    amount = IntegerField()
    price = DoubleField()
    orderId = IntegerField()

    class Meta:
        database = db
        order_by = 'id'
        db_table = 'ordered_products'
