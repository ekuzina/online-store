import sys
import os.path
from PyQt5.QtCore import QUrl, QObject, pyqtSlot, pyqtProperty

class Product(QObject):
    def __init__(self, product_id, name, image_first, image_second, rating, quantity, price, quantityinbasket = 0):
        super(Product, self).__init__()
        self.product_id = int(product_id)
        self.name = name
        self.image_first = image_first
        self.image_second = image_second
        self.rating = rating
        self.quantity = quantity
        self.price = price
        self.quantityinbasket = quantityinbasket

    @pyqtSlot(result = int)
    def getProductId(self):
        return self.product_id

    @pyqtSlot(result = str)
    def getProductName(self):
        return self.name

    @pyqtSlot(result = str)
    def getProductImage_first(self):
        return self.image_first

    @pyqtSlot(result = str)
    def getProductImage_second(self):
        return self.image_second

    @pyqtSlot(result = float)
    def getProductRating(self):
        return self.rating

    @pyqtSlot(result = int)
    def getProductPrice(self):
        return self.price

    @pyqtSlot(result = int)
    def getProductQuantity(self):
        return self.quantity

    @pyqtSlot(result = int)
    def getProductQuantityInBasket(self):
        return self.quantityinbasket

    @pyqtSlot(int)
    def updateProductQuantityInBasket(self, quantity):
        self.quantityinbasket = quantity

